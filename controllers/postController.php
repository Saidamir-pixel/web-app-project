<?php
session_start();
include "../../controllers/appController.php";
include "../../models/post.php";

//data output controll
function index(){
    $post = user_post();
    return $post;
}

//edit data controll
function edited(){
    $post = show();
    return $post;
}

//update data controll
function updated(){
    $post = update();
    return $post;
}

//delete data controll
function deleted(){
    $post = delete();
    return $post;
}


