<?php
include "../../controllers/appController.php";
include "../../models/user.php";

//authorization controll
function auth(){
    $auth = authorization();
    return $auth;
}
//login controll
function login() {
    session_start();
    $login = user_login();
    return $login;
}
//logout 
function logout() {
    if(isset($_GET['logout'])) {
        $url = '/';    
        session_destroy();
        header('Location: '.$url);
        exit;
    }
}
?>

