<?php 
include('../../controllers/postController.php');
index();
?>
<!-- create button -->
<?php require('../../views/layout/head.php') ?>
<body>
    <a style="max-width: 100px; margin: 10px;" class="btn btn-lg btn-primary btn-block my-2" href="index.php">Back</a>
    <div class="main_block">
        <form action="" class="form-signin" method="POST">
                <input type="text" name="title" class="form-control my-2" placeholder="Title" required>
                <textarea rows="7" name="description" class="form-control my-2" placeholder="Comment" required></textarea>
                <button class="btn btn-lg btn-primary btn-block my-2" type="submit">Save</button>
        </form>
    </div>
</body>
</html>

